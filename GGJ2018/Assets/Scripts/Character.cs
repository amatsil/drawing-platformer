﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : Life {

	Rigidbody2D RB;
	BoxCollider2D playerCollider;
    //Vector2 positionToCheck;
    public Vector3 speed;
    public Vector2 speedFactor;
	//public Vector2 direction = new Vector2(0,0);
	bool isGrounded;
	public float maxFallingDistance;
    bool isCheckCharacterFall = true;
    Vector3 fallStartPos;
    public bool isMove = true;

    public bool isReachedGoal = false;
	//public GameObject zombieGameObject;
    public ZombieDesigner zombieDesigner;
	// Use this for initialization
	void Start () {

        Register(true);
            
        

        if (RB == null) RB = GetComponent<Rigidbody2D> (); 
		playerCollider = GetComponent<BoxCollider2D> ();
        StartCoroutine(MoveCoroutine());
        StartCoroutine(CheckCharacterPositionCoroutine());
        StartCoroutine(CheckCharacterFallCoroutine());
	}

    void Register(bool isRegister)
    {
        if (isRegister)
        {
            CharactersManager.S.Characters.Add(this);
			if (ActionsPool.characterActions.OnCharacterSpawn != null) ActionsPool.characterActions.OnCharacterSpawn(this);

			ActionsPool.characterActions.OnCharacterFallAndDie += OnCharacterFallAndDie;
            ActionsPool.characterActions.OnCharacterReachGoal += OnCharacterReachGoal;


        }
        else
        {
            CharactersManager.S.Characters.Remove(this);

            ActionsPool.characterActions.OnCharacterFallAndDie -= OnCharacterFallAndDie;
            ActionsPool.characterActions.OnCharacterReachGoal -= OnCharacterReachGoal;

        }
    }

    private void OnCharacterReachGoal(Character character, Goal goal)
    {
        if (character == this)
        {
            isAlive = false;
            Register(false);

            Destroy(gameObject);
        }
        
    }

    private void OnCharacterFallAndDie(Character character)
    {
        if (character == this)
        KillCharacter(true);
    }

    private IEnumerator CheckCharacterFallCoroutine()
    {
        bool isFalling = false;
        while (isCheckCharacterFall)
        {
            if (!isFalling)
            {
                if (RB.velocity.y < -0.2f)
                {
                    isFalling = true;
                    fallStartPos = transform.position;
                }
            }
            else
            {
                if (RB.velocity.y > -0.2f)
                {
                    isFalling = false;
                    if (Mathf.Abs(transform.position.y - fallStartPos.y) > maxFallingDistance)
                        if (ActionsPool.characterActions.OnCharacterFallAndDie != null) ActionsPool.characterActions.OnCharacterFallAndDie(this);
                }
            }

            yield return null;
        }
    }

    internal void OnCharacterSwitchSide()
    {
        speed *= -1;
        zombieDesigner.flipChar();
    }
    
    private IEnumerator MoveCoroutine()
    {
        while (isMove)
        {
            transform.position +=  Time.deltaTime * speed;
            yield return new WaitForFixedUpdate();
        }
    }
    
    float minY = 0;
    IEnumerator CheckCharacterPositionCoroutine()
    {
        while (true)
        {
            if (transform.position.y < minY)
                KillCharacter(true);

            yield return new WaitForSeconds(1f);
        }
    }

    public void KillCharacter(bool isAction)
    {
        StartCoroutine(KillcharacterCoroutine(isAction));
    }
    
    IEnumerator KillcharacterCoroutine(bool isAction)
    {
        yield return null;
        if (isAlive)
        {
            isAlive = false;
            Register(false);
            if (isAction) if (ActionsPool.characterActions.OnCharacterKilled != null) ActionsPool.characterActions.OnCharacterKilled(this);
            Destroy(gameObject);
        }
    }

	public string layerName;
	
	public void SetSpeed(Vector2 dir){
        speed = dir * speed.x;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        Killer killer = coll.transform.GetComponent<Killer>();

        if (killer != null)
            KillCharacter(true);

        else
        {
            Goal goal = coll.transform.GetComponentInParent<Goal>();
            if (goal != null)
            {
                if (!isReachedGoal && isAlive)
                {
                    isReachedGoal = true;
                    Debug.Log("Character reached goal");
                    if (ActionsPool.characterActions.OnCharacterReachGoal != null) ActionsPool.characterActions.OnCharacterReachGoal(this, goal);
                }

            }
        }

    }
}
