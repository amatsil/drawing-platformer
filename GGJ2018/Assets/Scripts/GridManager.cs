﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GridManager : MonoBehaviour {

    public int maxGridOnAllowed;
    public int currentNumberOfGridOn;

    public List<GridCollider> gridHistory = new List<GridCollider>();

    public static GridManager S;

   
    public List<GridCollider> GridColliders = new List<GridCollider>();

    void Awake()
    {
        if (S != null) DestroyImmediate(gameObject);
        else S = this;
    }

	void Start () {


        ActionsPool.inputActions.OnGameObjectPressed += OnGameObjectPressed;
        ActionsPool.inputActions.OnMouseColliderInputUp += OnMouseColliderInputUp;
    
        ActionsPool.gridActions.OnGridPressed += OnGridPressed;
        ActionsPool.gameActions.OnBoardCompleted += OnBoardCompleted;

    }

    
    private void OnBoardCompleted()
    {
        GridColliders.ForEach((gc) => gc.gameCollider.gameObject.SetActive(false));

    }

    private void OnMouseColliderInputUp(Vector2 pos)
    {
        GridColliders.ForEach((gc) => gc.isChanged = false);
        isTurnOn = 0;
    }

    
    public int isTurnOn;

    private void OnGridPressed(GridCollider gc)
    {
        if (isTurnOn == 0)
            if (gc != null)
                isTurnOn = (gc.isOn ? 2 : 1);// !gc.isOn;
            
        if (isTurnOn != 0) gc.OnGridPressed(isTurnOn);
    }

    private void OnGameObjectPressed(GameObject go)
    {
        //Debug.Log("GameObjectPreesed");
        GridCollider gc = go.GetComponentInParent<GridCollider>();
        if (gc != null)
        {
            if (ActionsPool.gridActions.OnGridPressed != null) ActionsPool.gridActions.OnGridPressed(gc);
           // Debug.Log("GridColliderPressed");
        }
    }

    public void Reset()
    {
        GridColliders.ForEach((gc) =>
        {
            gc.isChanged = false;
            gc.ChangeState(false);
            gridHistory.Clear();
        });
    }
}

