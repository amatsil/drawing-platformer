﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class LevelsManager : MonoBehaviour {

    public static LevelsManager S;

    public List<Level> Levels = new List<Level>();

    public bool isStartInHomePanel = true;

    public Level CurrentLevel
    {
        get
        {
            int index = 0;
            Levels.ForEach((l) =>
            {
                if (l.gameObject.activeSelf)
                {
                    index = Levels.IndexOf(l);
                    Debug.Log("currentLevel is " + index);
                }
            });

            return Levels[index];

        }

        set
        {
            LoadLevel(value);
        }
    }

    void Awake()
    {
        if (S != null) DestroyImmediate(gameObject);
        else S = this;
    }

	IEnumerator Start () {
        // SetLevels();

        ActionsPool.gameActions.OnBoardCompleted += OnBoardCompleted;

        yield return null;

        
            Debug.Log("registrerating LevelsManager");
            ActionsPool.gameActions.OnLevelGoalComplete += OnLevelGoalComplete;
        ActionsPool.gameActions.OnLevelFailed += OnLevelFailed;

        //LoadLevel(Levels[0]);
        //LoadLevel(CurrentLevel);

       
    }

    private void OnBoardCompleted()
    {
        StartGame();
    }

    void StartGame()
    {
        if (isStartInHomePanel)
        {
            UIManager.S.OnHomeButtonPressed();
        }
    }

    private void OnLevelFailed()
    {
        int currentLevel = 0;
        Levels.ForEach((l) =>
        {
            if (l.gameObject.activeSelf)
            {
                currentLevel = Levels.IndexOf(l);
            //    Debug.Log("currentLevel is " + currentLevel);
            }
        });
        
        LoadLevel(Levels[currentLevel]);
    }

    private void OnLevelGoalComplete()
    {
        LoadNextLevel();
    }

    void OnEnable()
    {
        SetLevels();
    }

    void SetLevels()
    {
        var tLevels = GetComponentsInChildren<Level>(true);
        Levels = tLevels.ToList();
    }

    public void LoadLevel(Level level)
    {
        level.goal.Reset();
        CharactersManager.S.Reset();
        GridManager.S.Reset();
        Debug.Log("Loading Level " + level.name);
        
        Levels.ForEach((l) =>
        {
            l.gameObject.SetActive(false);
        });

        level.gameObject.SetActive(true);
        level.spawner.OnStartSpawning();

        if (ActionsPool.uiActions.OnUpdateTarget != null) ActionsPool.uiActions.OnUpdateTarget(level.goal.TargetCharacters);

    }

    public void LoadNextLevel()
    {
        Debug.Log("Loading next Level");
        /* int currentLevel = 0;
         Levels.ForEach((l) =>
         {
             if (l.gameObject.activeSelf)
             {
                 currentLevel = Levels.IndexOf(l);
                 Debug.Log("currentLevel is " + currentLevel);
             }
         });
         currentLevel++;
         */
        int index = Levels.IndexOf(CurrentLevel);
        index++;
        if (index < Levels.Count)
        CurrentLevel = Levels[index];
        else
        {
            UIManager.S.OnHomeButtonPressed();
        }
        //LoadLevel(Levels[currentLevel]);
       // LoadLevel(Levels[2]);
    }

}
