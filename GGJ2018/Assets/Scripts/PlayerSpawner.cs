﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour {

    public bool isSpawn = false;

	public GameObject player;
	public int numberOfPlayersInWave = 5;
    public int numberOfWaves = 5;

    public Vector2 spawnDirection = Vector2.left;

    public float timeBetweenCharacterSpawn = 1f;
    public float timeBetweenWaves = 4f;

    public float DelayBeforeSpawn = 3f;

    void Start()
    {
        Register(true);
    }

    void Register(bool isRegister)
    {
        if (isRegister)
        {
            ActionsPool.gameActions.OnBoardCompleted += OnBoardCompleted;
            ActionsPool.gameActions.OnLevelFailed += OnLevelFailed;

        }
        else
        {
            ActionsPool.gameActions.OnBoardCompleted -= OnBoardCompleted;

        }
    }

    private void OnLevelFailed()
    {
        
    }

    private void OnBoardCompleted()
    {
        //StartCoroutine(SpawnPlayers());
    }

    public void OnStartSpawning()
    {
        isSpawn = true;
        StartCoroutine(SpawnCharactersCoroutine());
    }
    private IEnumerator SpawnCharactersCoroutine()
	{
        Debug.Log("Spawn " + name);
        yield return null;
        GoalManager.S.availableCharacters = numberOfWaves * numberOfPlayersInWave;
        if (ActionsPool.uiActions.OnUpdateAvailable != null) ActionsPool.uiActions.OnUpdateAvailable(GoalManager.S.availableCharacters);
        yield return new WaitForSeconds(DelayBeforeSpawn);
        if (isSpawn)
        {
            for (int j = 0; j < numberOfWaves; j++)
            {
                for (int i = 0; i < numberOfPlayersInWave; i++)
                {
                    GameObject playerObject = Instantiate(player, transform.position, Quaternion.identity) as GameObject;
                    Character playerController = playerObject.GetComponent<Character>();
                    if (playerController != null) playerController.SetSpeed(spawnDirection);
                    else Debug.Log("Player Not Found");
                    yield return new WaitForSeconds(timeBetweenCharacterSpawn);
                }
                yield return new WaitForSeconds(timeBetweenWaves);

            }
            yield return null;
        }
    }


}
