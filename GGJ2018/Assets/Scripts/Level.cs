﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class Level : MonoBehaviour {

    public PlayerSpawner spawner;
    public LevelGoal goal;

    public List<Transform> LevelObjects = new List<Transform>();
    LevelsManager manager;

    void Awake()
    {
        spawner = GetComponentInChildren<PlayerSpawner>(true);
        goal = GetComponentInChildren<LevelGoal>(true);
        manager = GetComponentInParent<LevelsManager>();

    }

    void Start()
    {
        
        SetLevelObjects();
        // manager.LoadLevel(this);

        
    }

    void OnEnable()
    {
        spawner = GetComponentInChildren<PlayerSpawner>(true);
        goal = GetComponentInChildren<LevelGoal>(true);

        manager = GetComponentInParent<LevelsManager>();

        //  SetLevelObjects();
        //   manager.LoadLevel(this);

        spawner.StopAllCoroutines();
        spawner.isSpawn = true;
      //  StartCoroutine(spawner.SpawnCharactersCoroutine());
      //  if (Application.isPlaying) goal.Register(true);
    }

    void OnDisable()
    {
        spawner = GetComponentInChildren<PlayerSpawner>(true);
        goal = GetComponentInChildren<LevelGoal>(true);

        spawner.isSpawn = false;
        
    }

    void SetLevelObjects()
    {
        LevelObjects = GetComponentsInChildren<Transform>().ToList();
       // LevelObjects.ForEach((l) => Debug.Log(l.name));
    }
	       
         
}
