﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawingManager : MonoBehaviour {

    [SerializeField]
    bool isReadDraw = true;

    [SerializeField]
    DrawableObject drawObject;

	void Start () {

        StartCoroutine(ReadDrawing());
        	
	}

    List<Vector2> pointObject = new List<Vector2>();

    private IEnumerator ReadDrawing()
    {
    while (isReadDraw)
        {
            if (Input.GetMouseButtonDown(0))
            {
                StartReading();
            }

            if (Input.GetMouseButton(0))
            {
                ReadPoint(Camera.main.ScreenToWorldPoint( Input.mousePosition));
            }

            if (Input.GetMouseButtonUp(0))
            {
                CreateObject();
            }



            yield return null;
        }
    }


    private void StartReading()
    {
        pointObject = new List<Vector2>();
    }

    private void CreateObject()
    {
        
    }

    private void ReadPoint(Vector2 position)
    {
        HandleLineRenderer(position);
        HandleCollider(position);
    }

    int index = 0;

    private void HandleCollider(Vector2 position)
    {
        if (index < drawObject.coll.GetTotalPointCount())
        {
            drawObject.coll.points[index] = position;
            index++;
        }

    }

    private void HandleLineRenderer(Vector2 position)
    {
        drawObject.line.positionCount++;
        drawObject.line.SetPosition(drawObject.line.positionCount - 1, position);

    }
}
