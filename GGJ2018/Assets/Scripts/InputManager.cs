﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {

    public bool isColliderInput;



    LayerMask mask;
    LayerMask characterMask;
    LayerMask finalMask;
    void Start()
    {
        mask = 1 << LayerMask.NameToLayer(mouseInputLayer);
        characterMask = 1 << LayerMask.NameToLayer("CharacterInput");
        finalMask = mask | characterMask;


        ActionsPool.inputActions.OnMouseColliderInput += OnMouseColliderInput;

        StartCoroutine(GetInput());
    }

    private void OnMouseColliderInput(Vector2 mousePos)
    {
        GameObject go = ClickSelect(mousePos);
        if (go != null)
        {
            //Debug.Log("FUCK YERAH!");
            if (ActionsPool.inputActions.OnGameObjectPressed != null) ActionsPool.inputActions.OnGameObjectPressed(go);
        }
    }

    private IEnumerator GetInput()
    {
        while (true)
        {
            if (isColliderInput)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    if (ActionsPool.inputActions.OnMouseColliderInputDown != null) ActionsPool.inputActions.OnMouseColliderInputDown(Camera.main.ScreenToWorldPoint(Input.mousePosition));


                }

                if (Input.GetMouseButton(0))
                {
                    if (ActionsPool.inputActions.OnMouseColliderInput != null) ActionsPool.inputActions.OnMouseColliderInput(Camera.main.ScreenToWorldPoint(Input.mousePosition));


                }

                if (Input.GetMouseButtonUp(0))
                {
                  //  Debug.Log("MouseButtonUp");
                    if (ActionsPool.inputActions.OnMouseColliderInputUp != null) ActionsPool.inputActions.OnMouseColliderInputUp(Camera.main.ScreenToWorldPoint(Input.mousePosition));


                }


            }

            yield return null;
        }
    }

    [SerializeField]
    string mouseInputLayer;

    GameObject ClickSelect(Vector2 mousePos)
    {
        //Converting Mouse Pos to 2D (vector2) World Pos
        //Vector2 rayPos = new Vector2(mousePos.x, mousePos.y);

        //Debug.Log(mask);
        //ContactFilter2D filter = new ContactFilter2D();
        //filter.SetLayerMask(mask);

        RaycastHit2D[] hits = Physics2D.RaycastAll(mousePos, Vector2.zero, 0f, finalMask.value);

        if (hits.Length > 0)
        {
            RaycastHit hit;
            bool isChar = false;
            // Debug.Log(hit.transform.name);
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].transform.GetComponent<Character>() != null)
                {
                    Debug.Log("characterPressed");
                    isChar = true;
                    
                }
            }
            if (!isChar)
            return hits[0].transform.gameObject;
        }
        return null;
    }
}
