﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CharactersManager : MonoBehaviour {

    public static CharactersManager S;

    public List<Character> Characters = new List<Character>();
    
    void Awake()
    {
        if (S != null) DestroyImmediate(gameObject);
        else S = this;
    }

    void Start()
    {
        ActionsPool.gameActions.OnLevelGoalComplete += Reset;
        ActionsPool.gameActions.OnLevelFailed +=Reset;

    }
    
    public void Reset()
    {
        Characters.ForEach((c) => c.KillCharacter(false));
    }

}


