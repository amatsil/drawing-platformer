﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public static class ActionsPool
{

    public static InputActions inputActions = new InputActions();
    public static CharacterActions characterActions = new CharacterActions();
    public static GridActions gridActions = new GridActions();
    public static GameActions gameActions = new GameActions();
    public static UIActions uiActions = new UIActions();
	public static MusicActions musicActions = new MusicActions();

    public class InputActions
    {
        public Action<Vector2> OnMouseColliderInput;
        public Action<Vector2> OnMouseColliderInputDown;
        public Action<Vector2> OnMouseColliderInputUp;


        public Action<GameObject> OnGameObjectPressed;

    }

    public class GridActions
    {
        public Action<GridCollider> OnGridPressed;

    }

    public class CharacterActions
    {
        public Action<Character, Wall> OnCharacterSwitchSide;

        public Action<Character, Goal> OnCharacterReachGoal;
        public Action<Character> OnCharacterKilled;
        public Action<Character> OnCharacterFallAndDie;
		public Action<Character> OnCharacterSpawn;
    }

    public class GameActions
    {
        public Action OnBoardCompleted;
        public Action OnLevelGoalComplete;
        public Action OnLevelFailed;

    }

    public class UIActions
    {
        public Action<int> OnUpdateTarget;
        public Action<int> OnUpdateAvailable;
    }

	public class MusicActions
	{
		public Action OnMusicBackroundPlay;
		public Action OnMusicBackroundStopPlay;
		public Action OnMusicAmbientPlay;
		public Action OnMusicAmbientStop;
		public Action OnMusicMenuPlay;
		public Action OnMusicMenuStop;



	}
}
