﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGoal : MonoBehaviour
{

    

    public int TargetCharacters;

   // public int AllowedCharacterToLose;
   // public int CharactersReachedGoal = 0;
  //  public int CharactersKilled;

        IEnumerator Start()
    {
        yield return null;
        if (ActionsPool.uiActions.OnUpdateTarget != null) ActionsPool.uiActions.OnUpdateTarget(TargetCharacters);

    }

    void OnEnable()
    {
        Register(true);
        
    }

    void OnDisable()
    {
        Register(false);
    }

     void Register(bool isRegister)
    {
        if (isRegister)
        {
            ActionsPool.characterActions.OnCharacterReachGoal += OnCharacterReachGoal;
            ActionsPool.characterActions.OnCharacterKilled += OnCharacterKilled;

          
            ActionsPool.gameActions.OnLevelFailed += OnLevelFailed;
        }
        else
        {
            ActionsPool.characterActions.OnCharacterReachGoal -= OnCharacterReachGoal;
            ActionsPool.characterActions.OnCharacterKilled -= OnCharacterKilled;

         
            ActionsPool.gameActions.OnLevelFailed -= OnLevelFailed;
        }
    }

    private void OnLevelFailed()
    {
        Debug.Log("Level Failed");
    }

    private void OnCharacterKilled(Character character)
    {
        GoalManager.S.CharactersKilled++;
        GoalManager.S.availableCharacters--;

        if (ActionsPool.uiActions.OnUpdateAvailable != null) ActionsPool.uiActions.OnUpdateAvailable(GoalManager.S.availableCharacters);


      //  Debug.Log("Character Killed = " + GoalManager.S.CharactersKilled);

        if (GoalManager.S.availableCharacters < TargetCharacters)
        {
            Register(false);

            Reset();
            if (ActionsPool.gameActions.OnLevelFailed != null) ActionsPool.gameActions.OnLevelFailed();
        }
    }

   
    private void OnCharacterReachGoal(Character character, Goal goal)
    {
        GoalManager.S.CharactersReachedGoal++;
        GoalManager.S.availableCharacters--;

        if (ActionsPool.uiActions.OnUpdateAvailable != null) ActionsPool.uiActions.OnUpdateAvailable(GoalManager.S.availableCharacters);
        if (ActionsPool.uiActions.OnUpdateTarget != null) ActionsPool.uiActions.OnUpdateTarget(TargetCharacters - GoalManager.S.CharactersReachedGoal);



      //  Debug.Log("Character reached goal = " + GoalManager.S.CharactersReachedGoal + " GoalManager " + name);
        if (TargetCharacters == GoalManager.S.CharactersReachedGoal)
        {
            Register(false);
            //goal.gameObject.SetActive(false);
            if (ActionsPool.gameActions.OnLevelGoalComplete != null) ActionsPool.gameActions.OnLevelGoalComplete();

            Reset();
        }

    }

    public void Reset()
    {
        GoalManager.S.CharactersReachedGoal = 0;
        GoalManager.S.CharactersKilled = 0;
    }
}
