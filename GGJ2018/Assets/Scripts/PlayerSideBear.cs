﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSideBear : MonoBehaviour {


	public BearController character;

	/*  void OnTriggerEnter2D(Collider2D coll)
    {
        Debug.Log("OnTriggerEnter2D");
        Wall wall = coll.transform.GetComponentInParent<Wall>();
        if (wall != null)
        {
            Debug.Log("WaLLL!!!");
            if (ActionsPool.characterActions.OnCharacterSwitchSide != null) ActionsPool.characterActions.OnCharacterSwitchSide(player, wall);
            player.OnCharacterSwitchSide();
        }
    }
    */


	bool isColl = false;

	void OnCollisionEnter2D(Collision2D coll)
	{
		if (character)
		{
			Killer killer = coll.transform.GetComponent<Killer>();
			if (killer != null)
			{
				//character.KillCharacter(true);
			}
			else
			{

				Wall wall = coll.transform.GetComponent<Wall>();
				if (wall != null) {
					if (!isColl)
						//BearController.OnCharacterSwitchSide ();
					character = transform.parent.gameObject.GetComponent<BearController>();
					character.OnCharacterSwitchSide ();
					isColl = true;
				}
			}
		}
		else Destroy(gameObject);
	}

	void OnCollisionExit2D(Collision2D coll)
	{
		isColl = false;
	}

}
