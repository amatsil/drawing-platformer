﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SoundManager : MonoBehaviour {

    public static SoundManager S;

    [Header("Clips:")]
    	public AudioClip mainMusic;
        public AudioClip ButtonPress;
        public AudioClip busDoorOpens;
    //    public AudioClip levelBackroundMusic;
        public AudioClip ambiance;
        public AudioClip levelVictory;
        public AudioClip levelLost;
      //  public AudioClip zombieFalling;
     //   public AudioClip zombieDrawing;
     //   public AudioClip zombieJibRish;
    //    public AudioClip zombieSpawn;
    //    public AudioClip predatorAttack;
    //    public AudioClip fallingOnSpikes;
        public AudioClip drawingBlock;
        public AudioClip erasingBlock;
    public AudioClip characterKilled;
    public AudioClip characterSpawned;

    [Header("Audios:")]

    public List<AudioSource> soundEffects = new List<AudioSource>(); // 0 = music | 1 = ambiance | 2 - 5 = SoundsEffect
    public AudioSource Music;
    public AudioSource Ambiance;


    // Use this for initialization

    void Awake()
    {
        S = this;
    }

	void Start () {

        // Player falling
        //ActionsPool.characterActions.OnCharacterFallAndDie += OnCharacterFallAndDie;
        ActionsPool.characterActions.OnCharacterKilled += (c) => PlaySoundEffect(0, characterKilled);
	//	ActionsPool.characterActions.OnCharacterSpawn += (c) => PlaySoundEffect(1, characterSpawned);


        // Music Actions 
        ActionsPool.musicActions.OnMusicBackroundPlay += OnMusicBackroundPlay;
		ActionsPool.musicActions.OnMusicBackroundStopPlay += OnMusicBackroundStopPlay;
		ActionsPool.musicActions.OnMusicAmbientPlay += OnMusicAmbientPlay;
		ActionsPool.musicActions.OnMusicAmbientStop += OnMusicAmbientStop;
		//ActionsPool.musicActions.OnMusicMenuPlay += OnMusicMenuPlay;
		//ActionsPool.musicActions.OnMusicMenuStop += OnMusicMenuStop;

        ActionsPool.gameActions.OnLevelGoalComplete += () => PlaySoundEffect(2, levelVictory);
        ActionsPool.gameActions.OnLevelFailed += () => PlaySoundEffect(2, levelLost);

        

    }


    public void PlaySoundEffect(int source, AudioClip clip)
    {
        soundEffects[source].clip = clip;
        soundEffects[source].Play();
    }

	

	void OnMusicBackroundPlay(){
		Music.Play ();
	}

	void OnMusicBackroundStopPlay(){
		Music.Stop ();
	}

	void OnMusicAmbientPlay(){
		Ambiance.Play ();
	}

	void OnMusicAmbientStop(){
		Ambiance.Stop ();
	}
    /*
	void OnMusicMenuPlay(){
		menunMusic.Play ();
	}

	void OnMusicMenuStop(){
		menunMusic.Stop ();
	}
    */
}
