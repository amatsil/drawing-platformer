﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowAfterTransform : MonoBehaviour {

    public Transform followAfterTransform;

	
    void Awake()
    {
        transform.parent = null;
    }
	void Update () {
        if (followAfterTransform != null) transform.position = followAfterTransform.position;
        else Destroy(gameObject);
	}
}
