﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSideCollider : MonoBehaviour {

    [SerializeField]
    Character character;
    
  /*  void OnTriggerEnter2D(Collider2D coll)
    {
        Debug.Log("OnTriggerEnter2D");
        Wall wall = coll.transform.GetComponentInParent<Wall>();
        if (wall != null)
        {
            Debug.Log("WaLLL!!!");
            if (ActionsPool.characterActions.OnCharacterSwitchSide != null) ActionsPool.characterActions.OnCharacterSwitchSide(player, wall);
            player.OnCharacterSwitchSide();
        }
    }
    */
    bool isColl = false;

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (character)
        {
            Killer killer = coll.transform.GetComponent<Killer>();
            if (killer != null)
            {
                character.KillCharacter(true);
            }
            else
            {

                Wall wall = coll.transform.GetComponent<Wall>();
                if (wall != null)
                {
                    if (ActionsPool.characterActions.OnCharacterSwitchSide != null) ActionsPool.characterActions.OnCharacterSwitchSide(character, wall);
                    if (!isColl) character.OnCharacterSwitchSide();
                    isColl = true;
                }
                else
                {
                    Goal goal = coll.transform.GetComponent<Goal>();
                    if (goal != null)
                    {
                        if (!character.isReachedGoal)
                        {
                            Debug.Log("Player Side Collider Reached Goal");
                            character.isReachedGoal = true;
                            if (ActionsPool.characterActions.OnCharacterReachGoal != null) ActionsPool.characterActions.OnCharacterReachGoal(character, goal);
                        }

                    }
                }
            }
        }
        else Destroy(gameObject);
    }

    void OnCollisionExit2D(Collision2D coll)
    {
        isColl = false;
    }

}
