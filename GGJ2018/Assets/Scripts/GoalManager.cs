﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalManager : MonoBehaviour {

    public static GoalManager S;

    //  public int TargetCharacters;
    //  public int AllowedCharacterToLose;
    public int availableCharacters;

    public int CharactersReachedGoal = 0;
    private int charactersKilled;
    public int CharactersKilled
    {
        get
        {
            return charactersKilled;
        }
        set
        {
            charactersKilled = value;
        }
    }

    
    void Awake()
    {
        S = this;
    }
    
   
  
}
