﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BearController : MonoBehaviour {


		Rigidbody2D RB;
		BoxCollider2D playerCollider;
		//Vector2 positionToCheck;
		public Vector3 speed;
		public Vector2 speedFactor;
		//public Vector2 direction = new Vector2(0,0);
		bool isGrounded;
		public float maxFallingDistance;
		bool isCheckCharacterFall = true;
		Vector3 fallStartPos;
		public bool isMove = true;

		public bool isReachedGoal = false;
		//public GameObject zombieGameObject;
		// Use this for initialization
		void Start () {




			if (RB == null) RB = GetComponent<Rigidbody2D> (); 
			playerCollider = GetComponent<BoxCollider2D> ();
			StartCoroutine(MoveCoroutine());
			StartCoroutine(CheckCharacterPositionCoroutine());
		}
		


		internal void OnCharacterSwitchSide()
		{
			speed *= -1;
			transform.localScale = new Vector3(transform.localScale.x *-1, transform.localScale.y, transform.localScale.z);
		}

		private IEnumerator MoveCoroutine()
		{
			while (isMove)
			{
				transform.position +=  Time.deltaTime * speed;
				yield return new WaitForFixedUpdate();
			}
		}

		float minY = 0;
		IEnumerator CheckCharacterPositionCoroutine()
		{
			while (true)
			{
			if (transform.position.y < minY) {
			}//KillCharacter(true);

				yield return new WaitForSeconds(1f);
			}
		}




		public string layerName;

		public void SetSpeed(Vector2 dir){
			speed = dir * speed.x;
		}

		


}
