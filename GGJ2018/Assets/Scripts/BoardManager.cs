﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour {

    public static BoardManager S;

    public Vector3 offSet;

    [SerializeField]
    GameObject GridColliderPrefab;

    public float CellSize;
    public int BoardWidth;
    public int BoardHeight;

    void Awake()
    {
        if (S != null) DestroyImmediate(gameObject);
        else S = this;
    }

    void Start () {

        CreateBoard();

    }

    
    private void CreateBoard()
    {
        StartCoroutine(CreateBoardCoroutine());

        
    }

	public Dictionary<Vector2, GameObject> cellsByPos = new Dictionary<Vector2, GameObject>();
	public Dictionary<GameObject, Vector2> cellsByName = new Dictionary<GameObject, Vector2>();


    private IEnumerator CreateBoardCoroutine()
    {
        for (int w = 0; w < BoardWidth; w++)
            for (int h = 0; h < BoardHeight; h++)
            {
				Vector3 cellPos = new Vector3(CellSize + w * CellSize, CellSize + h * CellSize, 0) + offSet;
				GameObject Cell = (GameObject)Instantiate(GridColliderPrefab, cellPos, Quaternion.identity, transform);
				string name = "Cell_" + w.ToString () + h.ToString ();
				Cell.name = name;
				cellsByPos [new Vector2(w,h)] = Cell;
				cellsByName [Cell] = new Vector2 (w, h);
				Cell.transform.localScale *= CellSize;
//                yield return null;
            }

        yield return null;
        if (ActionsPool.gameActions.OnBoardCompleted != null) ActionsPool.gameActions.OnBoardCompleted();

    }
}
