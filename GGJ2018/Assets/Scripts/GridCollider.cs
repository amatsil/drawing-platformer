﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCollider : MonoBehaviour {

    public bool isChanged = false;
    public bool isOn =false;
	SpriteRenderer rend;

    [Header("Colliders:")]
    public Collider2D gameCollider;
    [SerializeField]
    Collider2D mouseCollider;
    
    void Start()
    {
        mouseCollider.GetComponent<Renderer>().enabled = false;

		rend = gameCollider.GetComponent<SpriteRenderer>();

        Register(true);
    }

    void Register(bool isRegister)
    {
        if (isRegister)
        {
            GridManager.S.GridColliders.Add(this);
        }

        else
        {
            GridManager.S.GridColliders.Remove(this);

        }
    }

    internal void OnGridPressed(int isTurnOn)
    {
        if (!isChanged)
        {
            isChanged = true;
            if (isTurnOn == 1)
                isOn = true;//ColliderManager.S.isTurnOn;//!isOn;
            else isOn = false;

            ChangeState(isOn);


        }

    }

    public void ChangeState(bool isOn)
    {
        if (GridManager.S.maxGridOnAllowed > GridManager.S.gridHistory.Count)
        {
            if (isOn)
                GridManager.S.gridHistory.Add(this);
        }
        else
            if (isOn)
        {
            GridManager.S.gridHistory[0].ChangeState(false);
            GridManager.S.gridHistory.Add(this);
						
			
		}

		if (!isOn) {
			GridManager.S.gridHistory.Remove (this);
			SpriteRenderer spriteRend = gameCollider.gameObject.GetComponent<SpriteRenderer> ();
			Color spriteColor = spriteRend.color;
			spriteColor.a = 1.0f;
			spriteRend.color = spriteColor;
			GetGraphic (true);

		} else {

			// Get Alpha factor
			float minAlpha = 0.3f;
			//int facotr = Convert.ToInt32((255- minAlpha) / GridManager.S.gridHistory.Count());

			for (int i = GridManager.S.gridHistory.Count - 1; i >= 0; i--) {
				if (GridManager.S.gridHistory [i].gameCollider.gameObject.activeSelf) {
					Debug.Log (GridManager.S.gridHistory [i].gameObject.name);
					SpriteRenderer spriteRend = GridManager.S.gridHistory [i].gameCollider.gameObject.GetComponent<SpriteRenderer> ();
					Color spriteColor = spriteRend.color;
					Debug.Log ("Alpha:" + Convert.ToInt32(spriteColor.a));
					if (spriteColor.a > minAlpha) {
						
						Debug.Log ("AAAAAAAAAAAAAAAA");
						spriteColor.a = (spriteColor.a - ((1.0f - minAlpha) / GridManager.S.gridHistory.Count));
						spriteRend.color = spriteColor;
					} else {
						break;
					}
				}
			}

		}

        gameCollider.gameObject.SetActive(isOn);

		if (gameCollider.gameObject.activeSelf) {
			rend.sprite = GetGraphic ();
		}


    }


	public List<Sprite> grassSpirtes = new List <Sprite>();
	public Sprite dirtSprite;


	Sprite GetGraphic(bool remove=false){

		Sprite spriteToReturn;
		Vector2 cellPos = BoardManager.S.cellsByName[gameObject];
		if (cellPos.y < BoardManager.S.BoardHeight - 1) {
			GameObject upCellCollider = (BoardManager.S.cellsByPos [new Vector2 (cellPos.x, cellPos.y + 1)].transform.Find ("GameCollider").gameObject);
		
			if (upCellCollider.activeSelf) {
				spriteToReturn = dirtSprite;
			} else {
				spriteToReturn = grassSpirtes [UnityEngine.Random.Range (0, grassSpirtes.Count)];
			}
		} else {
			spriteToReturn = grassSpirtes [UnityEngine.Random.Range (0, grassSpirtes.Count)];
		}
		if (cellPos.y > 0) {
			GameObject LowerCellCollider = (BoardManager.S.cellsByPos [new Vector2 (cellPos.x, cellPos.y - 1)].transform.Find ("GameCollider").gameObject);
			if (LowerCellCollider.activeSelf) {
				SpriteRenderer tempRend = LowerCellCollider.GetComponent<SpriteRenderer> ();
				if (!remove) {
					tempRend.sprite = dirtSprite;
				} else {
					tempRend.sprite = grassSpirtes [UnityEngine.Random.Range (0, grassSpirtes.Count)];
				}
			}
		}
		return spriteToReturn;

	}

}
