﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieDesigner : MonoBehaviour {


	public List<Color> skinColors = new List<Color>();
	public List<Color> torsoColors = new List<Color>();
	public List<Color> hairColors = new List<Color>();
	public List<Color> pantsColors = new List<Color>();
	public List<Color> hatColors = new List<Color>();
	public List<Color> shoesColors = new List<Color>();


	public List<GameObject> skinGroup = new List<GameObject>();
	public List<GameObject> torsoGroup = new List<GameObject>();
	public List<GameObject> hairGroup = new List<GameObject>();
	public List<GameObject> pantsGroup = new List<GameObject>();
	public List<GameObject> shoesGroup = new List<GameObject>();
	public List<GameObject> hatGroup = new List<GameObject>();




	// Use this for initialization
	void Start () {
		if (UnityEngine.Random.value < .5)
			flipHat (hatGroup);
		
		SetupAllParts ();
		//ActionsPool.characterActions.OnCharacterSwitchSide += flipChar;
	}
	

	void SetupAllParts(){
		SetupColorsGroup (skinGroup, skinColors);
		SetupColorsGroup (torsoGroup, torsoColors);
		SetupColorsGroup (hairGroup, hairColors);
		SetupColorsGroup (pantsGroup, pantsColors);
		SetupColorsGroup (hatGroup, hatColors, false);
		SetupColorsGroup (shoesGroup, shoesColors);

	}

	public void flipChar()//Character zombie/*, Wall wall*/){
    {
        //Vector3 newScale = zombie.zombieGameObject.transform.localScale;
        Vector3 newScale = transform.localScale;

        newScale.x *= -1;
        //zombie.zombieGameObject.transform.localScale = newScale;
        transform.localScale = newScale;


    }

    void SetupColorsGroup(List<GameObject> group, List<Color> colors, bool forceAlpha=true){
		Color randomCol = colors[UnityEngine.Random.Range(0, colors.Count)];
		if (forceAlpha) randomCol.a = 255;
		foreach (GameObject obj in group){
			SpriteRenderer spriteRend = obj.GetComponent<SpriteRenderer> ();
			spriteRend.color = randomCol;
		}


	}

	void flipHat(List<GameObject> group){
		foreach (GameObject hat in group) {
			SpriteRenderer spriteRend = hat.GetComponent<SpriteRenderer> ();
			spriteRend.flipX = true;
			hat.transform.localPosition = new Vector2 (-1.3f, 2.24f);
			hat.transform.eulerAngles = new Vector3 (0, 0,-14.81f); 
		}
	}






}
