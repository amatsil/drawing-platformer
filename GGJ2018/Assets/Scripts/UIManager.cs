﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager S;

    public Text targetText;
    
    public Text availableText;
    public int available;

    public RectTransform PausePanel;
    public RectTransform HomePanel;

    public RectTransform Fail;
    public RectTransform Win;

    void Awake()
    {
        if (S != null) DestroyImmediate(gameObject);
        else S = this;
    }

    void Start()
    {
        ActionsPool.uiActions.OnUpdateAvailable += OnUpdateAvailable;
        ActionsPool.uiActions.OnUpdateTarget += OnUpdateTarget;

        ActionsPool.gameActions.OnLevelFailed += OnLevelFailed;
        ActionsPool.gameActions.OnLevelGoalComplete += OnGoalComplete;

    }

    private void OnGoalComplete()
    {
        StopAllCoroutines();
        StartCoroutine(ShowPanel(Win, 0.8f));
    }

    private void OnLevelFailed()
    {
        StopAllCoroutines();
        StartCoroutine( ShowPanel(Fail, 0.8f));

    }

    IEnumerator ShowPanel(RectTransform rect, float time)
    {
        Debug.Log("Show Panel");
        rect.gameObject.SetActive(true);

        yield return new WaitForSeconds(time);
        rect.gameObject.SetActive(false);

    }

    private void OnUpdateTarget(int obj)
    {
        targetText.text = obj.ToString();
    }

    private void OnUpdateAvailable(int obj)
    {
        available = obj;
        availableText.text = available.ToString();
    }

    public void OnPauseButtonPressed()
    {
        Time.timeScale = 0;
        HomePanel.gameObject.SetActive(false);
        PausePanel.gameObject.SetActive(true);

        SoundManager.S.PlaySoundEffect(1, SoundManager.S.ButtonPress);

    }

    public void OnPlayButtonPressed()
    {
        Time.timeScale = 1;

        LevelsManager.S.LoadLevel(LevelsManager.S.Levels[0]);
        HomePanel.gameObject.SetActive(false);
        PausePanel.gameObject.SetActive(false);

        SoundManager.S.PlaySoundEffect(1, SoundManager.S.ButtonPress);
    }

    public void OnHomeButtonPressed()
    {

        Fail.gameObject.SetActive(false);
        Win.gameObject.SetActive(false);

        LevelsManager.S.LoadLevel(LevelsManager.S.Levels[0]);
        Time.timeScale = 0;
        HomePanel.gameObject.SetActive(true);
        PausePanel.gameObject.SetActive(false);

        SoundManager.S.PlaySoundEffect(1, SoundManager.S.ButtonPress);

    }

    public void OnContinuePressed()
    {
        Time.timeScale = 1;
        HomePanel.gameObject.SetActive(false);
        PausePanel.gameObject.SetActive(false);

        SoundManager.S.PlaySoundEffect(1, SoundManager.S.ButtonPress);

    }

    public void OnRestartButtonPressed()
    {
        Time.timeScale = 1;
        HomePanel.gameObject.SetActive(false);
        PausePanel.gameObject.SetActive(false);
        LevelsManager.S.LoadLevel(LevelsManager.S.CurrentLevel);

        SoundManager.S.PlaySoundEffect(1, SoundManager.S.ButtonPress);

    }

    public void OnSkipButtonPressed()
    {
        Time.timeScale = 1;
        HomePanel.gameObject.SetActive(false);
        PausePanel.gameObject.SetActive(false);
        LevelsManager.S.LoadNextLevel();

        SoundManager.S.PlaySoundEffect(1, SoundManager.S.ButtonPress);

    }
}
