﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;

[ExecuteInEditMode]
public class MyCamera : MonoBehaviour {

    [SerializeField]
    BoardManager board;

    void OnEnable()
    {
        Start();
    }

	void Start () {

        if (board) transform.position = new Vector3(board.BoardWidth / 4, board.BoardHeight / 4, -10);


    }
	
	
}
