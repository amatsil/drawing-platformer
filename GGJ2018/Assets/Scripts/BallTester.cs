﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallTester : MonoBehaviour {

    Rigidbody2D rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

	void Start () {

        ActionsPool.inputActions.OnMouseColliderInputDown += OnMouseColliderDown;	
	}

    private void OnMouseColliderDown(Vector2 obj)
    {
        rb.gravityScale = 0.5f;
    }
}
